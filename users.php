<?php
error_reporting(-1);

echo '<h3>Пользователи</h3>';

include_once('connect_db.php');

if (isset($_GET['id']) AND !empty($_GET['id'])) {
    $query = 'SELECT * FROM users WHERE id = '.$_GET['id'];
} else { $query = "SELECT * FROM users"; }

$result = $mysql->query($query); if (!$result){ die($mysql->error); }

function drawTable($data) {

    $result = '';
    $result .= '<table border="1" cellpadding="10"><tr>';
    foreach($data[0] as $key => $val) {
        $result .= '<th>'.$key.'</th>';
    }
    $result.= '<th>Детали</th><th>Редактирование записей</th><th>Удаление записей</th>';
    $result .= '</tr>';
    foreach($data as $k => $row){
        $result .= '<tr>';
        if ($row['is_active'] == 'false') continue;
        foreach($row as $key => $val) {
            $result .= '<td>'.$val.'</td>';
        }
        $result .= '<td><a href="detailed_info.php?table=users&id='.$row['id'].'">Детали</a></td>
        <td><a class="in_develop" href="#">Редактировать</a></td>
        <td><a class="in_develop" href="#">Удалить</a></td>';
        $result .= '</tr>';
    }
    $result .= '</table>';

    return $result;

}

$data = $result->fetch_all(MYSQLI_ASSOC);

if (count($data)) {
    echo drawTable($data);
} else {
    echo 'Таблица пустая!';
}

echo '<a class="in_develop" href="#">Создать новую запись</a><br><a href="index.php">На главную</a>';
echo '<script src=script.js></script>';