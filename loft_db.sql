-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 27 2016 г., 15:30
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `loft_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_bin NOT NULL,
  `status` enum('show','hide') COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `title`, `status`) VALUES
(1, 'Мужская обувь', 'show'),
(2, 'Женская обувь', 'show'),
(3, 'Детская обувь', 'show'),
(4, 'Обувь для бега', 'show'),
(5, 'Обувь для тяжелой атлетики', 'show'),
(6, 'Обувь для бокса', 'hide'),
(7, 'Обувь для балета', 'show'),
(8, 'Специальная обувь', 'show'),
(9, 'Резиновые сапоги', 'hide'),
(10, 'Валенки', 'show');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `date_order` datetime NOT NULL,
  `status` enum('awaiting payment','paid','formed','sent','delivered') COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `id_user`, `date_order`, `status`) VALUES
(1, 1, '2015-01-14 04:10:00', 'delivered'),
(2, 1, '2015-01-18 15:10:00', 'delivered'),
(3, 4, '2016-01-20 16:10:00', 'sent'),
(4, 6, '2016-01-22 18:15:00', 'formed'),
(5, 8, '2016-02-16 17:00:00', 'paid'),
(6, 2, '2016-02-16 21:00:00', 'paid'),
(7, 10, '2016-02-01 20:00:00', 'delivered'),
(8, 9, '2016-02-23 00:00:00', 'awaiting payment'),
(9, 3, '2016-02-22 06:00:00', 'formed'),
(10, 2, '2016-02-22 17:00:00', 'sent');

-- --------------------------------------------------------

--
-- Структура таблицы `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `price` float NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `order_items`
--

INSERT INTO `order_items` (`id`, `id_order`, `id_product`, `price`, `count`) VALUES
(1, 1, 2, 5000, 2),
(2, 1, 1, 10000, 4),
(3, 2, 5, 3000, 1),
(4, 2, 4, 4000, 2),
(5, 3, 7, 9000, 3),
(6, 3, 2, 2500, 1),
(7, 4, 6, 3000, 1),
(8, 4, 3, 2500, 1),
(9, 5, 4, 4000, 2),
(10, 5, 1, 2500, 1),
(11, 6, 11, 3000, 1),
(12, 6, 5, 1600, 2),
(13, 7, 3, 2500, 1),
(14, 7, 10, 3500, 1),
(15, 8, 4, 4000, 2),
(16, 8, 9, 3000, 1),
(17, 9, 4, 2000, 1),
(18, 9, 7, 3000, 1),
(19, 10, 3, 5000, 2),
(20, 10, 8, 1600, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_catalog` smallint(6) NOT NULL,
  `title` text COLLATE utf8_bin NOT NULL,
  `mark` text COLLATE utf8_bin NOT NULL,
  `count` smallint(6) NOT NULL,
  `price` float NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `status` enum('available','not available') COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `id_catalog`, `title`, `mark`, `count`, `price`, `description`, `status`) VALUES
(1, 1, 'Ботинки кожаные', 'Ralf Ringer', 25, 1500, 'Просто замечательные ботинки, сделаны в России', 'available'),
(2, 1, 'Ботинки кожаные', 'Timberland', 20, 2500, 'Легендарная американская марка', 'available'),
(3, 2, 'Лабутены', 'Moda La France', 30, 2500, 'Восхитительные лабутены, не забудь купить штаны', 'available'),
(4, 2, 'Сапожки', 'Moda Italia', 15, 2000, 'Сапожки как сапожки', 'available'),
(5, 3, 'Кроссовочки', 'Nike', 50, 800, 'Кроссовочки на детей от 3х лет', 'available'),
(6, 4, 'Кроссовки', 'Adidas', 30, 3000, 'Самые модные, самая последняя модель Adidas', 'available'),
(7, 5, 'Штангетки', 'Reebok', 25, 3000, 'Укрепленная подошва, тройные липучки, гарантия 3 года', 'available'),
(8, 7, 'Балетки', 'Лебедь', 50, 800, 'Самые обыкновенные белые балетки', 'available'),
(9, 8, 'Снегоступы', 'Polar Bear', 30, 3000, 'Made in Russia', 'available'),
(10, 8, 'Болотные сапоги', 'Traveller Monster', 40, 3500, 'Made in Canada', 'available'),
(11, 10, 'Валенки', 'Ded Egorich', 40, 2000, 'Не стильно, зато тепло', 'available');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext COLLATE utf8_bin NOT NULL,
  `lastname` tinytext COLLATE utf8_bin NOT NULL,
  `birthday` date NOT NULL,
  `email` tinytext COLLATE utf8_bin NOT NULL,
  `password` tinytext COLLATE utf8_bin NOT NULL,
  `is_active` enum('true','false') COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `birthday`, `email`, `password`, `is_active`) VALUES
(1, 'Елена', 'Рассохина', '1990-12-03', 'rassohina90@mail.ru', 'rassohina-alenka', 'true'),
(2, 'Дмитрий', 'Бабушкин', '1985-03-25', 'babushkin_d@gmail.com', 'babushki-oladushki', 'true'),
(3, 'Олег', 'Трофимов', '1988-05-26', 'trofim@list.ru', 'bugagashechka', 'true'),
(4, 'Екатерина', 'Свиридова', '1987-04-15', 'sviridovna@rambler.ru', 'sviridka03', 'true'),
(5, 'Елена', 'Васильева', '1985-09-01', 'vasilisa85@gmail.com', 'mydogisrotveller', 'false'),
(6, 'Алексей', 'Брыляков', '1986-03-05', 'aleks66@ya.ru', 'suzukina', 'true'),
(7, 'Степан', 'Хохтунов', '1986-08-12', 'hohtun_s@bk.ru', 'ghyu1029', 'false'),
(8, 'Ирина', 'Смирнова', '1988-05-04', 'smirnova-girl@ya.ru', 'superbabygirl', 'true'),
(9, 'Михаил', 'Безмолвный', '1989-02-25', 'bezmolvniy_misha@mail.ru', 'fishwithtrish', 'true'),
(10, 'Андрей', 'Папаев', '1991-01-05', 'dedpapai@bk.ru', 'qwerty123', 'true');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
