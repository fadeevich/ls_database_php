<?php

error_reporting(-1);

echo '<h3>Детальная информация</h3>';

include_once('connect_db.php');

if (isset($_GET['table']) and !empty($_GET['table'])) {
    $table = $_GET['table'];
} else { echo 'Что то пошло не так!'; exit; }
if (isset($_GET['id']) and !empty($_GET['id'])) {
    $id = $_GET['id'];
} else { echo 'Что то пошло не так!'; exit; }

switch($table) {
    case 'category':
        $query = 'SELECT products.title, products.mark, products.count, products.price, products.description FROM category LEFT JOIN products ON category.id = products.id_catalog WHERE category.id ='.$id;
        break;
    case 'products':
        $query = 'SELECT products.title, products.mark, order_items.price, order_items.count, users.name, users.lastname, orders.status, orders.date_order FROM order_items LEFT JOIN products ON products.id = order_items.id_product LEFT JOIN orders ON orders.id = order_items.id_order LEFT JOIN users ON users.id = orders.id_user WHERE products.id = ' . $id;
        break;
    case 'users':
        $query = 'SELECT users.name, users.lastname, products.title, products.mark, order_items.count, order_items.price, orders.date_order, orders.status FROM order_items LEFT JOIN products ON products.id = order_items.id_product LEFT JOIN orders ON orders.id = order_items.id_order LEFT JOIN users ON users.id = orders.id_user WHERE users.id = ' . $id;
        break;
    case 'orders':
        $query = 'SELECT users.name, users.lastname, products.title, products.mark, order_items.count, order_items.price, orders.date_order, orders.status FROM order_items LEFT JOIN products ON products.id = order_items.id_product LEFT JOIN orders ON orders.id = order_items.id_order LEFT JOIN users ON users.id = orders.id_user WHERE orders.id = ' . $id;
        break;
    case 'order_items':
        $id_product = $_GET['id_product'];
        $query = 'SELECT users.name, users.lastname, products.title, products.mark, orders.date_order, orders.status FROM order_items LEFT JOIN products ON products.id = order_items.id_product LEFT JOIN orders ON orders.id = order_items.id_order LEFT JOIN users ON users.id = orders.id_user WHERE orders.id = ' . $id . ' AND products.id = ' . $id_product;
        break;
    default:
        echo 'Что то пошло не так!';
        exit;
}

$result = $mysql->query($query); if (!$result){ die($mysql->error); }

function drawTable($data) {

    $result = '';
    $result .= '<table border="1" cellpadding="10"><tr>';
    foreach($data[0] as $key => $val) {
        $result .= '<th>'.$key.'</th>';
    }
    $result .= '</tr>';
    foreach($data as $k => $row){
        $result .= '<tr>';
        foreach($row as $key => $val) {
            $result .= '<td>'.$val.'</td>';
        }
        $result .= '</tr>';
    }
    $result .= '</table>';

    return $result;

}

$data = $result->fetch_all(MYSQLI_ASSOC);

if (count($data)) {
    echo drawTable($data);
} else {
    echo 'Таблица пустая!';
}

echo '<a href="index.php">На главную</a>';